<?php

use App\Http\Controllers\Admin\BlogController;
use App\Http\Controllers\Admin\BrandController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\CountryController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Frontend\BlogController as FrontendBlogController;
use App\Http\Controllers\Frontend\CartController;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\ProductController;
use App\Http\Controllers\Frontend\UserController as FrontendUserController;
use App\Http\Controllers\MailController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/m', function () {
    return view('welcome');
});

//Admin
Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::prefix('admin')->middleware(['admin'])->group(function(){
    Route::get('home',[DashboardController::class,'index']);
    Route::get('profile',[UserController::class,'index']);
    Route::post('profile',[UserController::class,'store']);

    //Country
    Route::get('country',[CountryController::class,'index']);
    Route::get('country/add',[CountryController::class,'create']);
    Route::post('country/add',[CountryController::class,'store']);
    Route::get('country/delete/{id}',[CountryController::class,'destroy']);

    //Category
    Route::get('category',[CategoryController::class,'index']);
    Route::get('category/add',[CategoryController::class,'create']);
    Route::post('category/add',[CategoryController::class,'store']);
    Route::get('category/delete/{id}',[CategoryController::class,'destroy']);

    //Brand
    Route::get('brand',[BrandController::class,'index']);
    Route::get('brand/add',[BrandController::class,'create']);
    Route::post('brand/add',[BrandController::class,'store']);
    Route::get('brand/delete/{id}',[BrandController::class,'destroy']);

    //Blog
    Route::get('blog',[BlogController::class,'index']);
    Route::get('blog/add',[BlogController::class,'create']);
    Route::post('blog/add',[BlogController::class,'store']);
    Route::get('blog/edit/{id}',[BlogController::class,'edit']);
    Route::post('blog/edit/{id}',[BlogController::class,'update']);
    Route::get('blog/delete/{id}',[BlogController::class,'destroy']);
});

//User
Route::get('/',[HomeController::class,'index']);
Route::prefix('account')->middleware(['memberNotLogin'])->group(function(){
    Route::get('register',[FrontendUserController::class,'create']);
    Route::post('register',[FrontendUserController::class,'register']);
    Route::get('login',[FrontendUserController::class,'showlogin']);
    Route::post('login',[FrontendUserController::class,'login']);
});
Route::get('account/logout',[FrontendUserController::class,'logout']);

Route::prefix('account')->middleware(['user'])->group(function(){
    Route::get('member',[FrontendUserController::class,'show']);
    Route::post('member',[FrontendUserController::class,'update']);
    Route::get('myproduct',[ProductController::class,'myproduct']);
    Route::get('addproduct',[ProductController::class,'addproduct']);
    Route::post('addproduct',[ProductController::class,'store']);
    Route::get('myproduct/edit/{id}',[ProductController::class,'edit']);
    Route::post('myproduct/edit/{id}',[ProductController::class,'update']);
    Route::get('myproduct/delete/{id}',[ProductController::class,'delete']);
});


Route::get('blog-list',[FrontendBlogController::class,'index']);
Route::get('blog-single/{id}',[FrontendBlogController::class,'detail']);
Route::post('blog-single/{id}',[FrontendBlogController::class,'comment']);
Route::post('blog/rate/ajax',[FrontendBlogController::class,'rate']);
// Route::post('blog/comment/ajax',[FrontendBlogController::class,'comment']);
// Route::post('blog/comment/replay/ajax',[FrontendBlogController::class,'replaycomment']);
Route::get('product/detail/{id}',[HomeController::class,'productdetail']);
Route::post('add-to-cart',[HomeController::class,'addtocart']);
Route::get('cart',[CartController::class,'mycart']);
Route::post('cart-quantity-up',[CartController::class,'upcart']);
Route::post('cart-quantity-down',[CartController::class,'downcart']);
Route::post('cart-quantity-delete',[CartController::class,'deletecart']);
Route::get('checkout',[CartController::class,'checkout']);
Route::post('checkout',[MailController::class,'sendmail']);
Route::get('send',[MailController::class,'index']);
Route::get('product/all',[ProductController::class,'get_all_product']);
Route::post('search',[HomeController::class,'search_name']);
Route::get('search-advanced',[HomeController::class,'search_advanced']);
Route::post('search-advanced',[HomeController::class,'result_search_advanced']);
Route::post('search-price',[HomeController::class,'search_price']);
Route::post('quick-signup',[CartController::class,'quick_signup']);