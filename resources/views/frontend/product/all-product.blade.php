@extends('frontend.layouts.app')
@section('content')
<div class="features_items"><!--features_items-->
	<h2 class="title text-center">Features Items</h2>
    @foreach($products as $product)
    @php 
		$image = json_decode($product->image);
	@endphp
	<div class="col-sm-4">
		<div class="product-image-wrapper">
			<div class="single-products">
				<div class="productinfo text-center">
					<img src="{{asset('upload/product/'.$product->id_user.'/'.$image[0].'')}}" alt="" />
					<h2>${{$product->price}}</h2>
					<p>{{$product->name}}</p>
					<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
				</div>
				<div class="product-overlay">
					<div class="overlay-content">
						<h2>${{$product->price}}</h2>
						<a href="{{ URL('product/detail/'.$product->id.'') }}"><p>{{$product->name}}</p></a>
						<a id="{{$product->id}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
					</div>
				</div>
			</div>
			<div class="choose">
				<ul class="nav nav-pills nav-justified">
					<li><a href=""><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
					<li><a href=""><i class="fa fa-plus-square"></i>Add to compare</a></li>
				</ul>
			</div>
		</div>
	</div>					
    @endforeach
    <ul class="pagination">
		<li>{{$products->links('pagination::bootstrap-4')}}</li>
	</ul>
</div>
<script>
	if(screen.width <= 736){
		document.getElementById("viewport").setAttribute("content", "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no");
	}	
	$(document).ready(function(){
		// $("form").submit(function(){
		// 	var productId = $("#productId").val();
		// 	console.log(productId);
		// });
		$("a.add-to-cart").click(function(){
			var id_product = $(this).attr('id');

			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				type: 'post',
				url: '{{ URL("add-to-cart") }}',
				data: {
					id_product: id_product
				},

				success:function(res){																			
					console.log(res)
					$("span#cart").text(res);
				}
			});
		});
	});
</script>
@endsection