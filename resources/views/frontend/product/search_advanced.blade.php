@extends('frontend.layouts.app')
@section('content')
<div class="features_items"><!--features_items-->
	<h2 class="title text-center">Features Items</h2>
    <div class="signup-form">
        <form action="" method="POST">
            @csrf
            <input name="name" type="text" placeholder="Name">
            <select name="price">
                <option value="">Choose price</option>
                <option value="0-150">$0 - $150</option>
                <option value="150-500">$150 - $500</option>
                <option value="500">%500-max</option>
            </select>
            <select name="id_category">
                <option value="">Category</option>
                @foreach($categories as $category)
                <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>
            <select name="id_brand">
                <option value="">Brand</option>
                @foreach($brands as $brand)
                <option value="{{$brand->id}}">{{$brand->name}}</option>
                @endforeach
            </select>
            <select name="status">
                <option value="">Status</option>
                <option value="0">New</option>
                <option value="1">Sale</option>
            </select>
            <button class="btn btn-primary" type="submit">Search</button>
        </form>
    </div> 
    <br>  
    @if(isset($products))
    @foreach($products as $product)
    @php 
		$image = json_decode($product->image);
	@endphp
	<div class="col-sm-4">
		<div class="product-image-wrapper">
			<div class="single-products">
				<div class="productinfo text-center">
					<img src="{{asset('upload/product/'.$product->id_user.'/'.$image[0].'')}}" alt="" />
					<h2>${{$product->price}}</h2>
					<p>{{$product->name}}</p>
					<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
				</div>
				<div class="product-overlay">
					<div class="overlay-content">
						<h2>${{$product->price}}</h2>
						<a href="{{ URL('product/detail/'.$product->id.'') }}"><p>{{$product->name}}</p></a>
						<a id="{{$product->id}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
					</div>
				</div>
			</div>
			<div class="choose">
				<ul class="nav nav-pills nav-justified">
					<li><a href=""><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
					<li><a href=""><i class="fa fa-plus-square"></i>Add to compare</a></li>
				</ul>
			</div>
		</div>
	</div>					
    @endforeach  
    <ul class="pagination">
		<li>{{$products->links('pagination::bootstrap-4')}}</li>
	</ul> 
    @endif             
</div><!--features_items-->
<style>
    .login-form form input, 
    .signup-form form input, 
    .signup-form form select, 
    .signup-form form textarea{
        width: 19%;
        display: inline-block;
    }
</style>
<script>
	if(screen.width <= 736){
		document.getElementById("viewport").setAttribute("content", "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no");
	}	
	$(document).ready(function(){
		// $("form").submit(function(){
		// 	var productId = $("#productId").val();
		// 	console.log(productId);
		// });
		$("a.add-to-cart").click(function(){
			var id_product = $(this).attr('id');

			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				type: 'post',
				url: '{{ URL("add-to-cart") }}',
				data: {
					id_product: id_product
				},

				success:function(res){																			
					console.log(res)
					$("span#cart").text(res);
				}
			});
		});
	});
</script>
@endsection