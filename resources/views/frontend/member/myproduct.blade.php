@extends('frontend.layouts.app')
@section('content')
<section id="cart_items">  
	<div class="table-responsive cart_info"> 
    @if(count($products)==0)
    <div class="text-center">
        Hiện tại chưa có mặt hàng nào
    </div>
    @else    
        <table class="table table-condensed">
            <thead>
                <tr class="cart_menu">
                    <td class="image">Id</td>
                    <td class="description">Name</td>
                    <td class="price">Image</td>
                    <td class="quantity">Price</td>
                    <td class="total">Action</td>
                    <td></td>
                </tr>
            </thead>
            <tbody>
                @foreach($products as $product)
                @php 
                    $image = json_decode($product->image);
                @endphp
                <tr>
                    <td>{{$product->id}}</td>
                    <td class="cart_description">{{$product->name}}</td>
                    <td class="cart_price">
                        <img src="{{asset('upload/product/'.$product->id_user.'/'.$image[0].'')}}" style="width: 50px;">
                    </td>
                    <td class="cart_total">{{$product->price}}</td>
                    <td>
                        <a href="{{ URL('account/myproduct/edit/'.$product->id.'') }}"><i class="fa fa-edit"></i></a>
                    </td>
                    <td>
                        <a class="cart_quantity_delete" href="{{ URL('account/myproduct/delete/'.$product->id.'') }}"><i class="fa fa-times"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>  
        @endif
    <a href="{{ URL('account/addproduct') }}" class="btn btn-primary pull-right">Add New</a>        
	</div>
</section>
@endsection