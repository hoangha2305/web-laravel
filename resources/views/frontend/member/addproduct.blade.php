@extends('frontend.layouts.app')
@section('content')
<div class="blog-post-area">
	<div class="col-sm-8">
        <div class="signup-form"><!--sign up form-->
            <h2>Create product!</h2>
            @if(session('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                {{session('success')}}
            </div>
            @endif
                            
            @if($errors->any())
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form action="" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="text" name="name" placeholder="Name" />
                <input type="text" name="price" placeholder="Price"/>
                <select name="id_category">
                    <option>Please choose category</option>
                    @foreach($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
                <select name="id_brand">
                    <option>Please choose brand</option>
                    @foreach($brands as $brand)
                    <option value="{{$brand->id}}">{{$brand->name}}</option>
                    @endforeach
                </select>
                <select id="select" name="status">
                    <option value="0">New</option>
                    <option value="1">Sale</option>
                </select>
                <div id="sale">
                    <input type="text" name="sale" placeholder="0">
                    <p>%</p>
                </div>
                <input type="text" name="company" placeholder="Company profile"/>
                <input type="file" name="image[]" multiple>
                <textarea name="detail" placeholder="Detail"></textarea>
                <button type="submit" class="btn btn-default">Add</button>
            </form>
        </div><!--/sign up form-->
    </div>
</div>
<script>
    $(document).ready(function(){
        $("#sale").hide();
        $("#select").change(function(){
            if($(this).val()=='0'){
                $("#sale").hide();
            }else{
                $("#sale").show();
            }
        });
    });
</script>
@endsection