@extends('frontend.layouts.app')
@section('content')
<div class="blog-post-area">
	<div class="col-sm-8">
        <div class="signup-form"><!--sign up form-->
            <h2>Update product!</h2>
            @if(session('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                {{session('success')}}
            </div>
            @endif
                            
            @if($errors->any())
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @foreach($products as $product)
            @php 
                $image = json_decode($product->image);
            @endphp
            <form action="" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="text" name="name" value="{{$product->name}}"/>
                <input type="text" name="price" value="{{$product->price}}"/>
                <select name="id_category">
                    <option>Please choose category</option>
                    @foreach($categories as $category)
                    <option value="{{$category->id}}" {{ ($category->id==$product->id_category) ? 'selected' : '' }} >{{$category->name}}</option>
                    @endforeach
                </select>
                <select name="id_brand">
                    <option>Please choose brand</option>
                    @foreach($brands as $brand)
                    <option value="{{$brand->id}}" {{ ($brand->id==$product->id_brand) ? 'selected' : '' }}>{{$brand->name}}</option>
                    @endforeach
                </select>
                <select id="select" name="status">
                    <option value="0" {{($product->status==0) ? 'selected' : '' }} >New</option>
                    <option value="1" {{($product->status==1) ? 'selected' : '' }}>Sale</option>
                </select>
                <div id="sale">
                    <input type="text" id="percent" name="sale" value="{{$product->sale}}">
                    <p>%</p>
                </div>
                <input type="text" name="company" value="{{$product->company}}"/>
                @foreach($image as $hinh)
                <div>
                    <img src="{{asset('upload/product/'.$product->id_user.'/'.$hinh.'')}}" width="50px">
                    <input type="checkbox" name="hinhxoa[]" value="{{$hinh}}">
                </div>
                @endforeach
                <br>
                <input type="file" name="image[]" multiple>
                <textarea name="detail">{{$product->detail}}</textarea>
                <button type="submit" class="btn btn-default">Update</button>
            </form>
            @endforeach
        </div><!--/sign up form-->
    </div>
</div>
<script>
    $(document).ready(function(){
        if($("#select").val()=='0'){
            $("#sale").hide();
        }else{
            $("#sale").show();
        }
        
        $("#select").change(function(){
            if($(this).val()=='0'){
                $("#sale").hide();
                $("input#percent").val(0);
            }else{
                $("#sale").show();
            }
        });
    });
</script>
@endsection