@extends('frontend.layouts.app')
@section('content')
<div class="blog-post-area">
	<h2 class="title text-center">Users</h2>
	<div class="col-sm-8">
		<div class="signup-form"><!--sign up form-->
			<h2>User updated!</h2>
			@if(session('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                {{session('success')}}
            </div>
            @endif
                            
            @if($errors->any())
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
            @endif
			@foreach($users as $user)
			<form action="" method="POST" enctype="multipart/form-data">
				@csrf
				<input type="text" name="name" value="{{$user->name}}"/>
				<input type="email" name="email" value="{{$user->email}}" readonly/>
				<input type="number" name="phone" value="{{$user->phone}}"/>
				<input type="text" name="address" value="{{$user->address}}"/>
				<input type="password" name="password" placeholder="Password"/>
                <select name="id_country" class="form-control form-control-line">
                    @foreach($countrys as $country)                        
                    <option value="{{$country->id}}" {{ ($user->id_country==$country->id) ? 'selected' : '' }} >{{$country->name}}</option>
                    @endforeach
                </select>
				<img src="{{asset('upload/user/avatar/'.$user->avatar.'')}}" style="width: 70px;">
				<input type="file" name="avatar">
				<button type="submit" class="btn btn-default">Update</button>
			</form>
			@endforeach
		</div><!--/sign up form-->
    </div>
</div>
@endsection

				