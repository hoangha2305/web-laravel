@extends('frontend.layouts.app')
@section('content')
                    <div class="blog-post-area">
						<h2 class="title text-center">Latest From our Blog</h2>
						@foreach($blogs as $blog)
						<div class="single-blog-post">
							<h3>{{$blog->title}}</h3>
							<div class="post-meta">
								<ul>
									<li><i class="fa fa-user"></i> Mac Doe</li>
									<li><i class="fa fa-clock-o"></i> 1:33 pm</li>
									<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
								</ul>
								<span>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star-half-o"></i>
								</span>
							</div>
							<a href="">
								<img src="{{asset('upload/blog/'.$blog->image.'')}}" alt="">
							</a>
							<p>{{$blog->description}}.</p>
							<a  class="btn btn-primary" href="/project/public/blog-single/{{$blog->id}}">Read More</a>
						</div>
						@endforeach
						<div class="pagination-area">
							<ul class="pagination">
								<li>{{$blogs->links('pagination::bootstrap-4')}}</li>
							</ul>
						</div>
					</div>
@endsection