@extends('frontend.layouts.app')
@section('content')
					<div class="blog-post-area">
						<h2 class="title text-center">Latest From our Blog</h2>
						<div class="single-blog-post">
							<h3>{{$data->title}}</h3>
							<div class="post-meta">
								<ul>
									<li><i class="fa fa-user"></i> Mac Doe</li>
									<li><i class="fa fa-clock-o"></i> 1:33 pm</li>
									<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
								</ul>
								<!-- <span>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-half-o"></i>
								</span> -->
							</div>
							<a href="">
								<img src="{{asset('upload/blog/'.$data->image.'')}}" alt="">
							</a>
							<p>
								{!!$data->content!!} <br>
							</p>
							<div class="pager-area">
								<ul class="pager pull-right">
									@if(isset($previous))
									<li><a href="/project/public/blog-single/{{$previous}}">Pre</a></li>
									@endif
									@if(isset($next))
									<li><a href="/project/public/blog-single/{{$next}}">Next</a></li>
									@endif
								</ul>
							</div>
						</div>
					</div><!--/blog-post-area-->

					<div class="rating-area">
						<ul class="ratings">
							<li class="rate-this">Rate this item:</li>
							<div class="rate">
								<div class="vote">
									@for($i=1;$i<=5;$i++)
									<div class="star_1 ratings_stars @if($i<=$medium) ratings_over @endif"><input value="{{$i}}" type="hidden"></div>
									@endfor
									<span class="rate-np">{{$medium}}</span>
								</div> 
							</div>
							<li class="color">({{$count}} votes)</li><br>
						</ul>
						<ul class="tag">
							<li>TAG:</li>
							<li><a class="color" href="">Pink <span>/</span></a></li>
							<li><a class="color" href="">T-Shirt <span>/</span></a></li>
							<li><a class="color" href="">Girls</a></li>
						</ul>
					</div><!--/rating-area-->

					<div class="socials-share">
						<a href=""><img src="images/blog/socials.png" alt=""></a>
					</div><!--/socials-share-->

					<!-- <div class="media commnets">
						<a class="pull-left" href="#">
							<img class="media-object" src="images/blog/man-one.jpg" alt="">
						</a>
						<div class="media-body">
							<h4 class="media-heading">Annie Davis</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							<div class="blog-socials">
								<ul>
									<li><a href=""><i class="fa fa-facebook"></i></a></li>
									<li><a href=""><i class="fa fa-twitter"></i></a></li>
									<li><a href=""><i class="fa fa-dribbble"></i></a></li>
									<li><a href=""><i class="fa fa-google-plus"></i></a></li>
								</ul>
								<a class="btn btn-primary" href="">Other Posts</a>
							</div>
						</div>
					</div> --><!--Comments-->
					<div class="response-area">
						<h2>3 RESPONSES</h2>
						<ul class="media-list">
							<!-- @foreach($comments as $comment)
								@if($comment->level==0)
									<li class="media" id="{{$comment->commentid}}">
										
										<a class="pull-left" href="#">
											<img class="media-object" src="images/blog/man-two.jpg" alt="">
										</a>
										<div class="media-body">
											<ul class="sinlge-post-meta">
												<li><i class="fa fa-user"></i>{{$comment->name}}</li>
												<li><i class="fa fa-clock-o"></i> 1:33 pm</li>
												<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
											</ul>
											<p>{{$comment->content}}</p>
											<a class="btn btn-primary" ><i class="fa fa-reply"></i>Replay</a><div class="text-area">
											<div class="text-area reply" id="{{$comment->commentid}}">
												<form method="POST" onsubmit="return false">
													@csrf
													<textarea name="replay" id="replay" rows="3"></textarea>
													<input type="hidden" id="level" value="{{$comment->commentid}}">
													<button class="btn btn-primary" type="submit" id="submit" >Replay comment</a>
												</form>
											</div>
										</div>
									</li
							@foreach($comments as $replaycomment)
								@if($replaycomment->level==$comment->commentid)
									<li class="media second-media">
										<a class="pull-left" href="#">
											<img class="media-object" src="images/blog/man-three.jpg" alt="">
										</a>
										<div class="media-body">
											<ul class="sinlge-post-meta">
												<li><i class="fa fa-user"></i>{{$replaycomment->name}}</li>
												<li><i class="fa fa-clock-o"></i> 1:33 pm</li>
												<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
											</ul>
											<p>{{$replaycomment->content}}</p>
										</div>
									</li>								
								@endif
							@endforeach	
								<li class="media second-media">													
									
								</li>							
							@endif
							@endforeach -->
							
							<!-- <li class="media second-media">
								<a class="pull-left" href="#">
									<img class="media-object" src="images/blog/man-three.jpg" alt="">
								</a>
								<div class="media-body">
									<ul class="sinlge-post-meta">
										<li><i class="fa fa-user"></i>Janis Gallagher</li>
										<li><i class="fa fa-clock-o"></i> 1:33 pm</li>
										<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
									</ul>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
									<a class="btn btn-primary" href=""><i class="fa fa-reply"></i>Replay</a>
								</div>
							</li>
							<li class="media second-media">
								<a class="pull-left" href="#">
									<img class="media-object" src="images/blog/man-three.jpg" alt="">
								</a>
								<div class="media-body">
									<ul class="sinlge-post-meta">
										<li><i class="fa fa-user"></i>Janis Gallagher</li>
										<li><i class="fa fa-clock-o"></i> 1:33 pm</li>
										<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
									</ul>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
									<a class="btn btn-primary" href=""><i class="fa fa-reply"></i>Replay</a>
								</div>
							</li>
							<li class="media second-media">
								<a class="pull-left" href="#">
									<img class="media-object" src="images/blog/man-three.jpg" alt="">
								</a>
								<div class="media-body">
									<ul class="sinlge-post-meta">
										<li><i class="fa fa-user"></i>Janis Gallagher</li>
										<li><i class="fa fa-clock-o"></i> 1:33 pm</li>
										<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
									</ul>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
									<a class="btn btn-primary" href=""><i class="fa fa-reply"></i>Replay</a>
								</div>
							</li> -->
							@foreach($comments as $comment)
								@if($comment->level==0)
								<li class="media" id="{{$comment->commentid}}">
									<a class="pull-left" href="#">
										<img class="media-object" src="images/blog/man-four.jpg" alt="">
									</a>
									<div class="media-body">
										<ul class="sinlge-post-meta">
											<li><i class="fa fa-user"></i>{{$comment->name}}</li>
											<li><i class="fa fa-clock-o"></i> 1:33 pm</li>
											<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
										</ul>
										<p>{{$comment->content}}</p>
										<a class="btn btn-primary" id="reply" href="#cmt"><i class="fa fa-reply"></i>Replay</a>
									</div>
								</li>
								@endif
								@foreach($comments as $replaycomment)
									@if($replaycomment->level==$comment->commentid)
									<li class="media second-media">
										<a class="pull-left" href="#">
											<img class="media-object" src="images/blog/man-three.jpg" alt="">
										</a>
										<div class="media-body">
											<ul class="sinlge-post-meta">
												<li><i class="fa fa-user"></i>{{$replaycomment->name}}</li>
												<li><i class="fa fa-clock-o"></i> 1:33 pm</li>
												<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
											</ul>
											<p>{{$replaycomment->content}}</p>
											<a class="btn btn-primary" href=""><i class="fa fa-reply"></i>Replay</a>
										</div>
									</li>
									@endif
								@endforeach
							@endforeach
							<!-- <li class="media second-media">
								<a class="pull-left" href="#">
									<img class="media-object" src="images/blog/man-three.jpg" alt="">
								</a>
								<div class="media-body">
									<ul class="sinlge-post-meta">
										<li><i class="fa fa-user"></i>Janis Gallagher</li>
										<li><i class="fa fa-clock-o"></i> 1:33 pm</li>
										<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
									</ul>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
									<a class="btn btn-primary" href=""><i class="fa fa-reply"></i>Replay</a>
								</div>
							</li>
							<li class="media second-media">
								<a class="pull-left" href="#">
									<img class="media-object" src="images/blog/man-three.jpg" alt="">
								</a>
								<div class="media-body">
									<ul class="sinlge-post-meta">
										<li><i class="fa fa-user"></i>Janis Gallagher</li>
										<li><i class="fa fa-clock-o"></i> 1:33 pm</li>
										<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
									</ul>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
									<a class="btn btn-primary" href=""><i class="fa fa-reply"></i>Replay</a>
								</div>
							</li>
							<li class="media second-media">
								<a class="pull-left" href="#">
									<img class="media-object" src="images/blog/man-three.jpg" alt="">
								</a>
								<div class="media-body">
									<ul class="sinlge-post-meta">
										<li><i class="fa fa-user"></i>Janis Gallagher</li>
										<li><i class="fa fa-clock-o"></i> 1:33 pm</li>
										<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
									</ul>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
									<a class="btn btn-primary" href=""><i class="fa fa-reply"></i>Replay</a>
								</div>
							</li> -->
						</ul>					
					</div><!--/Response-area-->
					<div class="replay-box">
						<div class="row">
							<div class="col-sm-12">
								<h2>Leave a replay</h2>
								
								<div class="text-area">
									<div class="blank-arrow">
										<label>Your Name</label>
									</div>
									<span>*</span>
									<form action="" method="POST" id="cmt">
										@csrf
										<textarea name="message" id="message" rows="11"></textarea>
										<input type="hidden" name="id_blog" value="{{$data->id}}">
										<input type="hidden" name="id_user" value="{{Auth::id()}}">
										<input type="hidden" name="level" id="level" value="0">
										<button class="btn btn-primary" type="submit" id="comment">post comment</button>
									</form>
								</div>
							</div>
						</div>
					</div><!--/Repaly Box-->
					<script>
						if(screen.width <= 736){
							document.getElementById("viewport").setAttribute("content", "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no");
						}	

						$(document).ready(function(){
							var isCheck = "{{Auth::check()}}";
							var id_user = "{{Auth::id()}}";
							var id_blog = "{{$data->id}}";
							//vote
							$('.ratings_stars').hover(
								// Handles the mouseover
								function() {
									$(this).prevAll().andSelf().addClass('ratings_hover');
									// $(this).nextAll().removeClass('ratings_vote'); 
								},
								function() {
									$(this).prevAll().andSelf().removeClass('ratings_hover');
									// set_votes($(this).parent());
								}
							);

							$('.ratings_stars').click(function(){
								var Values =  $(this).find("input").val();
								alert(Values);
								$.ajaxSetup({
									headers: {
										'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
									}
								});

								if ($(this).hasClass('ratings_over')) {
									$('.ratings_stars').removeClass('ratings_over');
									$(this).prevAll().andSelf().addClass('ratings_over');
								} else {
									$(this).prevAll().andSelf().addClass('ratings_over');
								}

								if(isCheck){
									$.ajax({
										type: 'post',
										url: '{{ URL("blog/rate/ajax") }}',
										data: {
											rate: Values,
											id_user: id_user,
											id_blog: id_blog
										},
									});
								
								}else{
									alert('Vui lòng đăng nhập');
								}
							});

							$("form").submit(function(){								
									if(isCheck){
										var content = $("#message").val();
										if(content==""){
											alert('Vui lòng nhập nội dung bình luận');
											return false;
										}
										return true;
									}else{
										alert('Vui lòng đăng nhập');
										
									}									
									return false;
							});

							$("a#reply").click(function(){
								var id = $(this).closest('li').attr('id');
								$('#level').val(id);
							});
							
						});

						// $(document).ready(function(){
						// 	var isCheck = "{{Auth::check()}}";
						// 	var id_user = "{{Auth::id()}}";
						// 	var id_blog = "{{$data->id}}";
						// 	//vote
						// 	$('.ratings_stars').hover(
						// 		// Handles the mouseover
						// 		function() {
						// 			$(this).prevAll().andSelf().addClass('ratings_hover');
						// 			// $(this).nextAll().removeClass('ratings_vote'); 
						// 		},
						// 		function() {
						// 			$(this).prevAll().andSelf().removeClass('ratings_hover');
						// 			// set_votes($(this).parent());
						// 		}
						// 	);

						// 	$('.ratings_stars').click(function(){
						// 		var Values =  $(this).find("input").val();
						// 		alert(Values);
						// 		$.ajaxSetup({
						// 			headers: {
						// 				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						// 			}
						// 		});

						// 		if ($(this).hasClass('ratings_over')) {
						// 			$('.ratings_stars').removeClass('ratings_over');
						// 			$(this).prevAll().andSelf().addClass('ratings_over');
						// 		} else {
						// 			$(this).prevAll().andSelf().addClass('ratings_over');
						// 		}

						// 		if(isCheck){
						// 			$.ajax({
						// 				type: 'post',
						// 				url: '{{ URL("blog/rate/ajax") }}',
						// 				data: {
						// 					rate: Values,
						// 					id_user: id_user,
						// 					id_blog: id_blog
						// 				},
						// 			});
								
						// 		}else{
						// 			alert('Vui lòng đăng nhập');
						// 		}
						// 	});

						// 	//cmt
						// 	$('a#comment').click(function(){
						// 		var Content = $('#message').val();
						// 		$.ajaxSetup({
						// 			headers: {
						// 				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						// 			}
						// 		});
						// 		if(isCheck){
						// 			// alert(Content);
						// 			$.ajax({
						// 				type: 'post',
						// 				url: '{{ URL("blog/comment/ajax") }}',
						// 				data: {
						// 					content: Content,
						// 					id_user: id_user,
						// 					id_blog: id_blog
						// 				},
						// 				success:function(res){			
																			
						// 					console.log(res.content)
											
						// 					var html = 
						// 					"<li class='media'>"
						// 						+"<a class='pull-left' href='#'>"
						// 							+"<img class='media-object' src='images/blog/man-two.jpg' alt=''>"
						// 						+"</a>"
						// 						+"<div class='media-body'>"
						// 							+"<ul class='sinlge-post-meta'>"
						// 								+"<li><i class='fa fa-user'></i>"+res.name+"</li>"
						// 								+"<li><i class='fa fa-clock-o'></i> 1:33 pm</li>"
						// 								+"<li><i class='fa fa-calendar'></i> DEC 5, 2013</li>"
						// 							+"</ul>"
						// 							+"<p>"+res.content+"</p>"
						// 							+"<a class='btn btn-primary' href=''><i class='fa fa-reply'></i>Replay</a>"
						// 						+"</div>"
						// 					"</li>"										
						// 					$(".media-list").append(html);
											
						// 				}
						// 			});
						// 		}else{
						// 			alert('Vui lòng đăng nhập');
						// 		}
						// 	});

						// 	//Replay cmt
						// 	$("form").submit(function(event){
						// 		var content = $("textarea#replay").val();
						// 		var level =  $("#level").val();
						// 		$.ajaxSetup({
						// 			headers: {
						// 				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						// 			}
						// 		});
						// 		// console.log(replay)
						// 		if(isCheck){
						// 			$.ajax({
						// 				type: 'post',
						// 				url: '{{ URL("blog/comment/replay/ajax") }}',
						// 				data: {
						// 					content: content,
						// 					id_user: id_user,
						// 					id_blog: id_blog,
						// 					level: level
						// 				},
						// 				success:function(res){																						
						// 					var replay = 
						// 					"<li class='media second-media'>"
						// 						+"<a class='pull-left' href='#'>"
						// 							+"<img class='media-object' src='images/blog/man-three.jpg' alt=''>"
						// 						+"</a>"
						// 						+"<div class='media-body'>"
						// 							+"<ul class='sinlge-post-meta'>"
						// 								+"<li><i class='fa fa-user'></i>"+res.name+"</li>"
						// 								+"<li><i class='fa fa-clock-o'></i> 1:33 pm</li>"
						// 								+"<li><i class='fa fa-calendar'></i> DEC 5, 2013</li>"
						// 							+"</ul>"
						// 							+"<p>"+res.content+"</p>"
						// 						+"</div>"
						// 					+"</li>";
						// 					$(replay).insertAfter("li#"+level)
																			
						// 				}
						// 			});
						// 		}else{
						// 			alert('Vui lòng đăng nhập');
						// 		}
						// 	});
						// });
					</script>
@endsection
								
