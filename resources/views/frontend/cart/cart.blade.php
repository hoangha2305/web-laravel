@extends('frontend.layouts.app')
@section('content')

    <section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>						
                        @php
                            $total = 0;
                         @endphp
						@if(isset($products))
                        @foreach($products as $product)
                        @php
                            $image = json_decode($product['image']);
                            $price = $product['price'];
                            $qty = $product['qty'];
                            $sum = $price * $qty;
                            $total += $sum;
                        @endphp
						<tr id="{{$product['id']}}">
							<td class="cart_product">
								<a href=""><img src="{{asset('upload/product/'.$product['id_user'].'/hinh85_'.$image[0].'')}}" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href="">{{$product['name']}}</a></h4>
								<p>Web ID: 1089772</p>
							</td>
							<td class="cart_price">
								<p id="price">${{$price}}</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class="cart_quantity_up"> + </a>
									<input class="cart_quantity_input" type="text" name="quantity" value="{{$qty}}" autocomplete="off" size="2">
									<a class="cart_quantity_down"> - </a>
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">${{$sum}}</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete"><i class="fa fa-times"></i></a>
							</td>
						</tr>
                        @endforeach
						@endif
						<!-- <tr>
							<td class="cart_product">
								<a href=""><img src="images/cart/two.png" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href="">Colorblock Scuba</a></h4>
								<p>Web ID: 1089772</p>
							</td>
							<td class="cart_price">
								<p>$59</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class="cart_quantity_up" href=""> + </a>
									<input class="cart_quantity_input" type="text" name="quantity" value="1" autocomplete="off" size="2">
									<a class="cart_quantity_down" href=""> - </a>
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">$59</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
							</td>
						</tr>
						<tr>
							<td class="cart_product">
								<a href=""><img src="images/cart/three.png" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href="">Colorblock Scuba</a></h4>
								<p>Web ID: 1089772</p>
							</td>
							<td class="cart_price">
								<p>$59</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class="cart_quantity_up" href=""> + </a>
									<input class="cart_quantity_input" type="text" name="quantity" value="1" autocomplete="off" size="2">
									<a class="cart_quantity_down" href=""> - </a>
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">$59</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
							</td>
						</tr> -->
					</tbody>
				</table>
			</div>
		</div>
	</section> <!--/#cart_items-->

	<section id="do_action">
		<div class="container">
			<div class="heading">
				<h3>What would you like to do next?</h3>
				<p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="chose_area">
						<ul class="user_option">
							<li>
								<input type="checkbox">
								<label>Use Coupon Code</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Use Gift Voucher</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Estimate Shipping & Taxes</label>
							</li>
						</ul>
						<ul class="user_info">
							<li class="single_field">
								<label>Country:</label>
								<select>
									<option>United States</option>
									<option>Bangladesh</option>
									<option>UK</option>
									<option>India</option>
									<option>Pakistan</option>
									<option>Ucrane</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>
								
							</li>
							<li class="single_field">
								<label>Region / State:</label>
								<select>
									<option>Select</option>
									<option>Dhaka</option>
									<option>London</option>
									<option>Dillih</option>
									<option>Lahore</option>
									<option>Alaska</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>
							
							</li>
							<li class="single_field zip-field">
								<label>Zip Code:</label>
								<input type="text">
							</li>
						</ul>
						<a class="btn btn-default update" href="">Get Quotes</a>
						<a class="btn btn-default check_out" href="">Continue</a>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="total_area">
						<ul>
							<li>Cart Sub Total <span id="total">${{$total}}</span></li>
							<li>Eco Tax <span>$2</span></li>
							<li>Shipping Cost <span>Free</span></li>
							<li>Total <span>$61</span></li>
						</ul>
							<a class="btn btn-default update" href="">Update</a>
							<a class="btn btn-default check_out" href="{{ URL('checkout') }}">Check Out</a>
					</div>
				</div>
			</div>
		</div>
	</section><!--/#do_action-->
	<script>
		$(document).ready(function(){
			var total = $("span#total").text();
			total = parseInt(total.substr(1));
			if(screen.width <= 736){
				document.getElementById("viewport").setAttribute("content", "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no");
			}	
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			$("a.cart_quantity_up").click(function(){
				var qty = $(this).closest(".cart_quantity").find("input.cart_quantity_input").val();
				var id = $(this).closest("tr").attr("id");
				var price = $(this).closest("tr").find("p#price").text();
				var sum = 0;
				price = parseInt(price.substr(1));
				qty = parseInt(qty);
				qty += 1;
				sum = price*qty;
				total += price;
				$(this).closest(".cart_quantity").find("input.cart_quantity_input").val(qty);
				$(this).closest("tr").find(".cart_total_price").text("$"+sum);
				$("span#total").text("$"+total);

				$.ajax({
					type: 'post',
					url: '{{ URL("cart-quantity-up") }}',
					data: {
						id_product: id,
					},
				});
			});
			$("a.cart_quantity_down").click(function(){
				var qty = $(this).closest(".cart_quantity").find("input.cart_quantity_input").val();
				var id = $(this).closest("tr").attr("id");
				var price = $(this).closest("tr").find("p#price").text();
				var sum = 0;
				price = parseInt(price.substr(1));
				qty = parseInt(qty);
				qty -= 1;
				if(qty<1){
					$(this).closest("tr").hide();
				}
				sum = price*qty;
				total -= price;
				$(this).closest(".cart_quantity").find("input.cart_quantity_input").val(qty);
				$(this).closest("tr").find(".cart_total_price").text("$"+sum);
				$("span#total").text("$"+total);

				$.ajax({
					type: 'post',
					url: '{{ URL("cart-quantity-down") }}',
					data: {
						id_product: id
					},
				});
			});
			$("a.cart_quantity_delete").click(function(){
				var id = $(this).closest("tr").attr("id");
				var qty = $(this).closest("tr").find("input.cart_quantity_input").val();
				var price = $(this).closest("tr").find("p#price").text();
				var sum = 0;
				price = parseInt(price.substr(1));
				qty = parseInt(qty);
				sum = price * qty;
				total -= sum;

				$(this).closest("tr").hide();
				$("span#total").text("$"+total);
				$.ajax({
					type: 'post',
					url: '{{ URL("cart-quantity-delete") }}',
					data: {
						id_product: id
					},
				});
			});
		});
	</script>
@endsection