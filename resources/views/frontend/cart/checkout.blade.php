@extends('frontend.layouts.app')
@section('content')
    <section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Check out</li>
				</ol>
			</div><!--/breadcrums-->

			<div class="step-one">
				<h2 class="heading">Step1</h2>
			</div>
			<div class="checkout-options">
				<h3>New User</h3>
				<p>Checkout options</p>
				<ul class="nav">
					<li>
						<label><input type="checkbox"> Register Account</label>
					</li>
					<li>
						<label><input type="checkbox"> Guest Checkout</label>
					</li>
					<li>
						<a href=""><i class="fa fa-times"></i>Cancel</a>
					</li>
				</ul>
			</div><!--/checkout-options-->

			<div class="register-req">
				<p>Please use Register And Checkout to easily get access to your order history, or use Checkout as Guest</p>
			</div><!--/register-req-->

			@if(empty(Auth::check()))
			<div class="shopper-informations">
				<div class="row">
					<div class="signup-form"><!--sign up form-->
						<h2>New User Signup!</h2>
						@if(session('success'))
						<div class="alert alert-success alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
							<h4><i class="icon fa fa-check"></i> Thông báo!</h4>
							{{session('success')}}
						</div>
						@endif
									
						@if($errors->any())
						<div class="alert alert-danger alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
							<h4><i class="icon fa fa-check"></i> Thông báo!</h4>
							<ul>
								@foreach($errors->all() as $error)
								<li>{{$error}}</li>
								@endforeach
							</ul>
						</div>
						@endif
						<form action="{{ URL('quick-signup') }}" method="POST">
							@csrf
							<input type="text" name="name" id="name" placeholder="Name"/>
							<input type="email" name="email" id="email" placeholder="Email"/>
							<input type="password" name="password" id="password" placeholder="Password"/>
							<input type="text" name="phone" id="phone" placeholder="Phone"/>
							<input type="text" name="address" id="address" placeholder="Address">
							<select name="id_country" class="form-control form-control-line">
								@foreach($countrys as $country)
								<option value="{{$country->id}}">{{$country->name}}</option>
								@endforeach
							</select>
							<button type="submit" class="btn btn-default">Signup</button>
						</form>
					</div><!--/sign up form-->					
				</div>
			</div>
			@endif
			<div class="review-payment">
				<h2>Review & Payment</h2>
			</div>

			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
                        @if(isset($products))
						@php $subtotal = 0; @endphp
                        @foreach($products as $product)
                            @php
                                $image = json_decode($product['image']);
                                $total = $product['price']*$product['qty'];		
								$subtotal += $total;
                            @endphp
                            <tr>
                                <td class="cart_product">
                                    <a><img src="{{asset('upload/product/'.$product['id_user'].'/hinh85_'.$image[0].'')}}" alt=""></a>
                                </td>
                                <td class="cart_description">
                                    <h4><a href="">{{$product['name']}}</a></h4>
                                    <p>Web ID: 1089772</p>
                                </td>
                                <td class="cart_price">
                                    <p>${{$product['price']}}</p>
                                </td>
                                <td class="cart_quantity">
                                    <div class="cart_quantity_button">
                                        <a class="cart_quantity_up" href=""> + </a>
                                        <input class="cart_quantity_input" type="text" name="quantity" value="{{$product['qty']}}" autocomplete="off" size="2">
                                        <a class="cart_quantity_down" href=""> - </a>
                                    </div>
                                </td>
                                <td class="cart_total">
                                    <p class="cart_total_price">${{$total}}</p>
                                </td>
                                <td class="cart_delete">
                                    <a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                        @endforeach                      
						<tr>
							<td colspan="4">&nbsp;</td>
							<td colspan="2">
								<table class="table table-condensed total-result">
									<tr>
										<td>Cart Sub Total</td>
										<td>${{$subtotal}}</td>
									</tr>
									<tr>
										<td>Exo Tax</td>
										<td>$2</td>
									</tr>
									<tr class="shipping-cost">
										<td>Shipping Cost</td>
										<td>Free</td>										
									</tr>
									<tr>
										<td>Total</td>
										<td><span>$61</span></td>
									</tr>
									@if(!empty(session()->get('cart')))
                                    <tr>
                                        <td>
											<form action="" method="POST">
												@csrf
												<input type="hidden" name="user_id" value="{{Auth::id()}}">
												<input type="hidden" name="price" value="{{$subtotal}}">
												<button type="submit" class="btn btn-primary" id="checkout">Checkout</button>
											</form>
										</td>
                                    </tr>
									@endif
								</table>
							</td>
						</tr>
						@endif
					</tbody>
				</table>
			</div>
			<div class="payment-options">
					<span>
						<label><input type="checkbox"> Direct Bank Transfer</label>
					</span>
					<span>
						<label><input type="checkbox"> Check Payment</label>
					</span>
					<span>
						<label><input type="checkbox"> Paypal</label>
					</span>
				</div>
		</div>
	</section> <!--/#cart_items-->
	<script>
		$(document).ready(function(){	
			$("#checkout").click(function(){
				var isCheck = "{{Auth::check()}}";
				if(isCheck){
					return true;
				}else{
					$('html, body').animate({
                    	scrollTop: $("div.register-req").offset().top
                	}, 2000);
				}
				return false;
			});	
			
			$("form").submit(function(){
				var name = $("#name").val();
				var email = $("#email").val();
				var password = $("#password").val();
				var phone = $("#phone").val();
				var address = $("#address").val();

				if(name==""){
					alert("Vui lòng nhập tên");
					return false;
				}
				if(email==""){
					alert("Vui lòng nhập email");
					return false;
				}
				if(password==""){
					alert("Vui lòng nhập password");
					return false;
				}
				if(phone==""){
					alert("Vui lòng nhập phone");
					return false;
				}
				if(address==""){
					alert("Vui lòng nhập address");
					return false;
				}

				return true;
			});
		});
		
	</script>
@endsection