<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'image[]'=>'image|mimes:jpeg,png,jfif,jpg,gif|max:2048'
        ];
    }

    public function messages()
    {
        return [
            'mimes'=>':attribute phải đúng định dạng jpeg,png,jfif,jpg,gif',
            'image'=>':attribute phải là hình ảnh',
            'max'=> ':attribute không được vượt quá kích thước 1MB'
        ];
    }
}
