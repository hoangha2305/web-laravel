<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BlogUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title'=>'required',
            'image'=>'image|mimes:jpeg,png,jpg,gif|max:2048',
            'description'=>'required',
            'content'=>'required'
        ];
    }

    public function messages()
    {
        return [
            'required'=>':attribute không được để trống',
            'mimes'=>':attribute phải đúng định dạng jpeg,png,jpg,gif',
            'image'=>':attribute phải là hình ảnh',
            'max'=> ':attribute không được vượt quá kích thước 1MB'
        ];
    }
}
