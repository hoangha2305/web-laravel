<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Faker\Guesser\Name;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    public function myproduct()
    {
        $id_user = Auth::id();
        $products = Product::select('id','name','image','price','id_user')->where('id_user',$id_user)->get();
        return view('frontend.member.myproduct',compact('products'));
    }

    public function addproduct()
    {
        $categories = Category::all();
        $brands = Brand::all();
        return view('frontend.member.addproduct',compact('categories','brands'));
    }

    public function store(ProductRequest $request)
    {
        $id = Auth::id();

        if(count($request->image)==3){
            if(!file_exists("upload/product/".$id)){           
                mkdir("upload/product/".$id);
            }
            if($request->hasFile('image')){
                foreach($request->file('image') as $image){
                    $data[] = $this->xulyanh($image,$id);
                }
            }
            $product = $request->all();
            $product['image'] = json_encode($data);
            $product['id_user'] = $id;
            if(Product::create($product)){
                return redirect()->back()->with('success',_('Add product success'));
            }else{
                return redirect()->back()->withErrors('Add product error');
            }
                  
        }else{
            return redirect()->back()->withErrors('Vui lòng nhập đủ 3 hình ảnh');
        }       
    }

    public function xulyanh($image,$id)
    {
        $name = $image->getClientOriginalName();
        $name_2 = "hinh329_".$image->getClientOriginalName();
        $name_3 = "hinh85_".$image->getClientOriginalName();

        $path = public_path('upload/product/'.$id.'/'.$name);
        $path2 = public_path('upload/product/'.$id.'/'.$name_2);
        $path3 = public_path('upload/product/'.$id.'/'.$name_3);

        Image::make($image->getRealPath())->save($path);
        Image::make($image->getRealPath())->resize(329,380)->save($path2);
        Image::make($image->getRealPath())->resize(85,84)->save($path3);

        return $name;
    }

    public function edit($id)
    {
        $products = Product::where('id',$id)->get();
        $categories = Category::all();
        $brands = Brand::all();
        return view('frontend.member.editproduct',compact('products','categories','brands'));
    }

    public function update(Request $request, $id)
    {
        $id_user = Auth::id();
        $products = Product::where('id',$id)->get();
        foreach($products as $product){
            $image = json_decode($product->image);
        }
        
        if($request->hinhxoa){
            foreach($request->hinhxoa as $value){
                if(in_array($value,$image)){  
                    $key = array_search($value,$image);
                    unset($image[$key]);
                }      
            }
            $image = array_values($image);
        }

        if($request->hasFile('image')){
            if(count($request->file('image'))>3 ||count($request->file('image'))+count($image)>3){
                return redirect()->back()->withErrors('Số lượng ảnh nhập vào và ảnh xóa nhỏ hơn hoặc bằng 3 hình ảnh');                  
            }else{           
                foreach($request->file('image') as $hinhnew){
                    $new[] = $this->xulyanh($hinhnew,$id_user);
                }
                $image = array_merge($image,$new);            
            }
        }
        $product_id = Product::findOrFail($id);
        $data = $request->all();
        $data['image'] = json_encode($image);
        $data['id_user'] = $id_user;

        if($product_id->update($data)){
            return redirect()->back()->with('success','Update product success');
        }else{
            return redirect()->back()->withErrors('Update product error');
        }      
    }

    public function delete($id)
    {
        $result = Product::where('id',$id)->delete();
        if($result){
            return redirect()->back();
        }
    }

    public function get_all_product()
    {
        $products = Product::paginate(6);
        return view('frontend.product.all-product',compact('products'));
    }
}
