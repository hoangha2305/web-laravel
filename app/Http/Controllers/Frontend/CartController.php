<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function mycart()
    {
        $products = session()->get('cart');
        return view('frontend.cart.cart',compact('products'));
    }

    public function upcart(Request $request)
    {
        $id = $request->id_product;

        if(session()->has('cart')){
            $getSession = session()->get('cart');
            foreach($getSession as $key => $value){
                if($value['id']==$id){ 
                    $getSession[$key]['qty'] += 1;
                    session()->put('cart',$getSession);
                }
            }
        }
    }

    public function downcart(Request $request)
    {
        $id = $request->id_product;

        if(session()->has('cart')){
            $getSession = session()->get('cart');
            foreach($getSession as $key => $value){
                if($value['id']==$id){ 
                    $getSession[$key]['qty'] -= 1;
                    if($getSession[$key]['qty']<1){
                        unset($getSession[$key]);
                    }
                    session()->put('cart',$getSession);
                }
            }
        }
    }

    public function deletecart(Request $request)
    {
        $id = $request->id_product;

        if(session()->has('cart')){
            $getSession = session()->get('cart');
            foreach($getSession as $key => $value){
                if($value['id']==$id){ 
                    unset($getSession[$key]);
                }
                session()->put('cart',$getSession);
            }         
        } 
    }

    public function checkout()
    {
        $countrys = Country::get();
        $products = session()->get('cart');
        return view('frontend.cart.checkout',compact('products','countrys'));
    }

    public function quick_signup(Request $request)
    {
        $user = $request->all();
        $user['password'] = bcrypt($request->password);
        $user['level'] = 0;
        if(User::create($user)){

        }else{
            return redirect()->back()->withErrors('Register Error');
        }

        $login = [
            'email' => $request->email,
            'password'=>$request->password,
            'level'=>0
        ];
        if(Auth::attempt($login)){
            return redirect()->back()->with('success',_('Login success, Please Checkout'));
        }else{
            return redirect()->back()->withErrors('Email or Password is not correct');
        }
    }
}
