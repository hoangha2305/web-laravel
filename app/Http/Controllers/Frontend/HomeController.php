<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cart = 0;
        if(session()->has('cart')){
            $cart = count(session()->get('cart'));
        }    
        $products = Product::orderBy('created_at','desc')->take(6)->get();
        return view('frontend.home',compact('products','cart'));
    }

    
    public function productdetail($id)
    {
        $products = Product::where('id',$id)->get();
        return view('frontend.product.product-detail',compact('products'));
    }

    public function addtocart(Request $request)
    {
        $id = $request->id_product;

        $product = Product::select('id','name','price','image','id_user')->where('id',$id)->get()->toArray(); 
        $product=$product[0];
        $product['qty'] = 1;

        $xx = 1;
        
        if(session()->has('cart')){
            $getSession = session()->get('cart');
            foreach($getSession as $key => $value){
                if($value['id']==$id){ 
                    $getSession[$key]['qty'] += 1;
                    session()->put('cart',$getSession);
                    $xx = 0;
                }
            }
        }
        if($xx==1){
            session()->push('cart',$product);
        }

        $cart = count(session()->get('cart'));
        return $cart;    
    }

    public function search_name(Request $request)
    {
        $name = $request->name;
        $products = Product::where('name','LIKE',"%{$name}%")->get();
        return view('frontend.product.search-name',compact('products'));
    }

    public function search_advanced()
    {
        $categories = Category::get();
        $brands = Brand::get();
        return view('frontend.product.search_advanced',compact('categories','brands'));
    }

    public function result_search_advanced(Request $request)
    {
        $categories = Category::get();
        $brands = Brand::get();
        $products = Product::query();
        if(!empty($request->name)){
            $products = $products->where('name','LIKE',"%{$request->name}%");
        }
        if($request->price!=null){
            $arr = explode("-",$request->price);
            $price1 = $arr[0];
            if(count($arr)==2){            
                $price2 = $arr[1];
                $products = $products->whereBetween('price',[$price1,$price2]);
            }else{
                $products = $products->where('price','>',$price1);
            }         
        }
        if(!empty($request->id_category)){
            $products = $products->where('id_category',$request->id_category);
        }
        if(!empty($request->id_brand)){
            $products = $products->where('id_brand',$request->id_brand);
        }
        if($request->status!=null){
            $products = $products->where('status',$request->status);
        }
        $products = $products->orderByDesc('created_at')->paginate(6);
        return view('frontend.product.search_advanced',compact('categories','brands','products'));
    }

    public function search_price(Request $request)
    {
        $value = $request->value;

        $value = explode(" : ",$value);
        $price1 = $value[0];
        $price2 = $value[1];

        $products = Product::whereBetween('price',[$price1,$price2])->orderByDesc('created_at')->get();

        return $products;
    }

}
