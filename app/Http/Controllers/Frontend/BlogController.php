<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\CommentBlog;
use App\Models\RateBlog;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BlogController extends Controller
{
    public function index()
    {
        $blogs = Blog::select('id','title','image','description')->orderByDesc('id')->paginate(2);
        return view('frontend.blog.blog-list',compact('blogs'));
    }

    public function detail($id)
    {
        $count = 0;
        $sum = 0;
        $medium = 0;
        $data = Blog::find($id);
        $previous = Blog::where('id','<',$data->id)->max('id');
        $next = Blog::where('id','>',$data->id)->min('id');
        $rates = RateBlog::where('id_blog',$id)->select('rate')->get();

        $comments = CommentBlog::join('users', 'blogcomment.id_user', '=', 'users.id')
        ->select('blogcomment.id as commentid','users.id as iduser','users.name as name','blogcomment.content as content','blogcomment.level as level')
        ->where('blogcomment.id_blog', $id)
        ->orderBy('blogcomment.id', 'DESC')
        ->get();

        if($rates){
            foreach($rates as $rate){
                $sum+=$rate->rate;
                $count++;
            }
            if($count>0){
                $medium = round($sum/$count,1);
            }
        }
        return view('frontend.blog.blog-single',compact('data','previous','next','medium','count','comments'));
    }

    public function rate(Request $request)
    {
        $rate = new RateBlog();
        $rate->id_user = $request->id_user;
        $rate->id_blog = $request->id_blog;
        $rate->rate = $request->rate;
        $rate->save();
    }

    public function comment(Request $request)
    {
        $comment = new CommentBlog();
        $comment->id_user = Auth::id();
        $comment->id_blog = $request->id_blog;
        $comment->content = $request->message;
        $comment->level = $request->level;
        $comment->save();

        return redirect()->back();
    }

    // public function replaycomment(Request $request)
    // {
    //     $comment = new CommentBlog();
    //     $comment->id_user = $request->id_user;
    //     $comment->id_blog = $request->id_blog;
    //     $comment->content = $request->content;
    //     $comment->level = $request->level;
    //     $comment->save();

    //     $user = User::find($comment->id_user);
    //     return response()->json(['content' => $comment->content,'name' => $user->name]);
    // }

}
