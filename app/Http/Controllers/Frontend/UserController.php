<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\FrontendUserRequest;
use App\Http\Requests\MemberLoginRequest;
use App\Http\Requests\MemberRequest;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\Country;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countrys = Country::all();
        return view('frontend.member.register',compact('countrys'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function register(RegisterRequest $request)
    {
        $users = new User();
        $users->name = $request->name;
        $users->email = $request->email;
        $users->password = bcrypt($request->password);
        $users->phone = $request->phone;
        $users->address = $request->address;
        $users->id_country = $request->id_country;
        $users->level = 0;
        $file = $request->avatar;
        if(!empty($file)){
            $users->avatar = $file->getClientOriginalName();
        }
        $users->save();
        if($users->save()){
            if(!empty($file)){
                $file->move('upload/user/avatar',$file->getClientOriginalName());
            }
            return redirect()->back()->with('success',_('Register Success'));
        }else{
            return redirect()->back()->withErrors('Register Error');
        }
    }

    public function showlogin()
    {
        return view('frontend.member.login');
    }

    public function login(MemberLoginRequest $request)
    {
        $login = [
            'email' => $request->email,
            'password'=>$request->password,
            'level'=>0
        ];

        $remember = false;
        if($request->remember_me){
            $remember = true;
        }
        if(Auth::attempt($login,$remember)){
            return redirect('/');
        }else{
            return redirect()->back()->withErrors('Email or Password is not correct');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('account/login');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $id = Auth::id();
        $users = User::where('id',$id)->get();
        $countrys = Country::all();
        return view('frontend.member.account',compact('users','countrys'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MemberRequest $request)
    {
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $data = $request->all();
        $file = $request->avatar;
        if(!empty($file)){
            $data['avatar'] = $file->getClientOriginalName();
        }
        if($data['password']){
            $data['password'] = bcrypt($data['password']);
        }else{
            $data['password'] = $user->password;
        }
        if($user->update($data)){
            if(!empty($file)){
                $file->move('upload/user/avatar',$file->getClientOriginalName());
            }
            return redirect()->back()->with('success',_('Update profile success'));
        }else{
            return redirect()->back()->withErrors('Update profile error');
        }
    }

}
