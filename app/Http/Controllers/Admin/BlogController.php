<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\BlogRequest;
use App\Http\Requests\BlogUpdateRequest;
use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $blogs = Blog::all();
        return view('admin.blog.index',compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blog.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogRequest $request)
    {
        $blog = $request->all();
        $file = $request->image;
        if(!empty($file)){
            $blog['image'] = $file->getClientOriginalName();
        }
        if(Blog::create($blog)){
            if(!empty($file)){
                $file->move('upload/blog',$file->getClientOriginalName());
            }
            return redirect()->back()->with('success',_('Add blog success'));
        }else{
            return redirect()->back()->withErrors('Add blog error');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::find($id);
        return view('admin.blog.edit',compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BlogUpdateRequest $request, $id)
    {
        $blog = Blog::findOrFail($id);
        $data = $request->all();
        $file = $request->image;
        if(!empty($file)){
            $data['image']=$file->getClientOriginalName();
        }else{
            $data['image']=$blog->image;
        }
        if($blog->update($data)){
            if(!empty($file)){
                $file->move('upload/blog',$file->getClientOriginalName());
            }
            return redirect()->back()->with('success',_('Update blog success'));
        }else{
            return redirect()->back()->withErrors('Update blog error');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = Blog::where('id',$id)->delete();
        if($result){
            return redirect()->back();
        }
    }
}
