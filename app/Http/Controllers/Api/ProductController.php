<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function index()
    {
        $product = Product::all();

        return response()->json(['product'=>$product]);
    }

    public function login(Request $request)
    {
        $login = [
            'email' => $request->email,
            'password'=>$request->password,
            'level'=>0
        ];

        if(Auth::attempt($login)){
            $id = Auth::user()->id;
            $user = User::where('id',$id)->get();
            return response()->json(['user'=>$user]);
        }else{
            return response()->json(['user'=>'That Bai']);
        }
    }
}
