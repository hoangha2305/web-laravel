<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailNotify;
use App\Models\History;
use App\Models\User;

class MailController extends Controller
{
    public function index()
    {
        $data = [
            'subject' => 'Cảm ơn đã đặt hàng',
            'body' => 'Hello This is my email delivery!'
        ];
        try {
            Mail::to('hha.20it10@vku.udn.vn')->send(new MailNotify($data));
            return response()->json(['Great check your mail box']);
        } catch (\Exception $th) {
            return $th->getMessage();
        }
    }

    public function sendmail(Request $request)
    {
        $price = $request->price;
        $user = User::find($request->user_id)->toArray(); 
        $email = $user['email'];
        $name = $user['name'];  
        $phone = $user['phone'];  
        $cart = session()->get('cart');

        $history = new History();
        $history->name = $name;
        $history->email = $email;
        $history->phone = $phone;
        $history->id_user = $request->user_id;
        $history->price = $price;
        $history->save();

        $data = [
            'subject' => 'Đặt hàng thành công',
            'cart' => $cart,
            'price' => $price
        ];

        try {
            Mail::to('hha.20it10@vku.udn.vn')->send(new MailNotify($data));
            return response()->json(['Great check your mail box']);
        } catch (\Exception $th) {
            return $th->getMessage();
        }
    }
}
