<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommentBlog extends Model
{
    use HasFactory;

    protected $table = 'blogcomment';

    protected $fillable = [
        'id',
        'id_user',
        'id_blog',
        'content',
        'level'
    ];
}
