<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RateBlog extends Model
{
    use HasFactory;

    protected $table = 'ratesblog';

    protected $fillable = [
        'id',
        'id_user',
        'id_blog',
        'rate'
    ];
}
